const assert = require("assert");
const app = require("../../src/app");

describe("'speech' service", () => {
  it("registered the service", () => {
    const service = app.service("speech");

    assert.ok(service, "Registered the service");
  });
});
