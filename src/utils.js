function streamToPromise(stream) {
  return new Promise(function(resolve, reject) {
    stream.on("data", resolve);
    stream.on("end", resolve);
    stream.on("error", reject);
    stream.resume();
  });
}

module.exports = { streamToPromise };
