const fs = require("fs");
const { textToSpeech } = require("../watson");

module.exports = function(options = {}) { // eslint-disable-line no-unused-vars
  return function writeTts(hook) {
    const formats = ["ogg", "mp3"];

    return Promise.all(formats.map(format => synthesizeAudio(hook, format)))
      .then(() => hook)
      .catch(err => { throw new Error(err); });

    function synthesizeAudio(hook, format) {
      const baseParams = {
        text: hook.data.text,
        voice: "en-US_AllisonVoice"
      };

      return new Promise((resolve, reject) => {
        try {
          const params = { ...baseParams, accept: `audio/${format}` };
          textToSpeech.synthesize(params).pipe(fs.createWriteStream(`public/audio/tts/${hook.result._id}.${format}`));
          resolve();
        } catch (err) {
          reject(err);
        }
      });
    }
  };
};
