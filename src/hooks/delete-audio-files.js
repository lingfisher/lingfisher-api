const fs = require("fs");

module.exports = function(options = {}) { // eslint-disable-line no-unused-vars
  return function deleteAudioFiles(hook) {
    // const locations = ["recorded", "tts"];
    const locations = ["tts"];  // don't accidentally destroy recorded audio for now
    const formats = ["ogg", "mp3"];
    const paths = generatePaths(hook.id, locations,formats);

    return Promise.all(paths.map(path => deleteFile(path)))
      .then(() => hook)
      .catch(err => { throw new Error(err); });

    function generatePaths(id, locations, formats) {
      // FIXME this is unreadable
      const f = (a, b) => [].concat(...a.map(d => b.map(e => [].concat(d, e))));
      const cartesian = (a, b, ...c) => b ? cartesian(f(a, b), ...c): a;
      return cartesian(locations, formats).map(e => `public/audio/${e[0]}/${id}.${e[1]}`);
    }

    function deleteFile(path) {
      return new Promise((resolve, reject) => {
        fs.unlink(path, (err) => {
          if (err) {
            if (err.code === "ENOENT") {  // don't worry about missing files
              resolve();
            }
            reject(err);
          }
          resolve();
        });
      });
    }
  };
};
