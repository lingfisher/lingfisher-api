const fs = require("fs");

module.exports = function(options = {}) { // eslint-disable-line no-unused-vars
  return function chooseAudioType(hook) {
    // NB only works on get, not find

    // check for recorded audio first
    return Promise.all([
      checkExists(`public/audio/recorded/${hook.result._id}.ogg`),
      checkExists(`public/audio/recorded/${hook.result._id}.mp3`)
    ])
      .then(() => {
        hook.result.audioType = "recorded";
        return hook;
      })
      .catch(err => {
        // if no recorded audio, check for tts
        if (err.code === "ENOENT") {
          return Promise.all([
            checkExists(`public/audio/tts/${hook.result._id}.ogg`),
            checkExists(`public/audio/tts/${hook.result._id}.mp3`)
          ])
          .then(() => {
            hook.result.audioType = "tts";
            return hook;
          })
          .catch(err => {
            throw new Error(err);
          });
        }
        throw new Error("err");
      });
  };

  function checkExists(path) {
    return new Promise((resolve, reject) => {
      fs.access(path, (err) => {
        if (err) {
          reject(err);
        }
        resolve();
      });
    });
  }
};
