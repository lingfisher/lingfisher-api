const multer = require("multer");
const mime = require("mime-types");
require("dotenv").config();

// upload from client, store in memory
const upload = multer({ storage: multer.memoryStorage() });

// upload to google cloud storage
const gcloud = require("google-cloud")({
  projectId: process.env.GOOGLE_CLOUD_PROJECT,
  credentials: JSON.parse(process.env.GOOGLE_CLOUD_AUTHENTICATION)
});
const storage = gcloud.storage({
  projectId: process.env.GOOGLE_CLOUD_PROJECT
});
const bucket = storage.bucket(process.env.GOOGLE_CLOUD_BUCKET);

function uploadToGCS(req, res, next) {
  if (!req.file) {
    return next();
  }

  const extension = mime.extension(req.body.mimeType);
  const gcsName = `${req.body.userId}_${req.body.sentenceId}_${Date.now()}.${extension}`;
  const file = bucket.file(gcsName);

  const stream = file.createWriteStream({
    metadata: {
      contentType: req.file.mimetype
    }
  });

  stream.on("error", (err) => {
    req.file.cloudStorageError = err;
    next(err);
  });

  stream.on("finish", () => {
    req.file.cloudStorageObject = gcsName;
    file.makePublic().then(() => {
      req.file.cloudStoragePublicUrl = getPublicUrl(gcsName);
      next();
    });
  });

  stream.end(req.file.buffer);

  function getPublicUrl(filename) {
    return `https://storage.googleapis.com/${process.env.GOOGLE_CLOUD_BUCKET}/${filename}`;
  }
}


module.exports = function() {
  const app = this; // eslint-disable-line no-unused-vars

  app.use("/speech", upload.single("speech"), uploadToGCS, (req, res, next) => {
    req.feathers.file = req.file;
    next();
  });
};
