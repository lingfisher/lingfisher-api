const SpeechToText = require("watson-developer-cloud/speech-to-text/v1");
const TextToSpeech = require("watson-developer-cloud/text-to-speech/v1");
require("dotenv").config();

const speechToText = new SpeechToText({
  username: process.env.SPEECH_TO_TEXT_USERNAME,
  password: process.env.SPEECH_TO_TEXT_PASSWORD
});

const textToSpeech = new TextToSpeech({
  username: process.env.TEXT_TO_SPEECH_USERNAME,
  password: process.env.TEXT_TO_SPEECH_PASSWORD
});

module.exports = { speechToText, textToSpeech };
