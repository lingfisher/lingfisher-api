const { authenticate } = require("feathers-authentication").hooks;

const chooseAudioType = require("../../hooks/choose-audio-type");
const writeTTS = require("../../hooks/write-tts");
const deleteAudioFiles = require("../../hooks/delete-audio-files");

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [authenticate("jwt")],
    update: [authenticate("jwt")],
    patch: [authenticate("jwt")],
    remove: [authenticate("jwt")]
  },

  after: {
    all: [],
    find: [],
    get: [chooseAudioType()],
    create: [writeTTS()],
    update: [],
    patch: [],
    remove: [deleteAudioFiles()]
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
