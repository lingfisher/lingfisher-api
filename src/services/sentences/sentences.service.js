// Initializes the `sentences` service on path `/sentences`
const createService = require("feathers-mongoose");
const createModel = require("../../models/sentences.model");
const hooks = require("./sentences.hooks");
const filters = require("./sentences.filters");

module.exports = function() {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get("paginate");

  const options = {
    name: "sentences",
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use("/sentences", createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service("sentences");

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
