/* eslint-disable no-unused-vars */
const errors = require("feathers-errors");
const uuid = require("node-uuid");

const amqp = require("amqplib");
const mime = require("mime-types");

const AMQP_URL = process.env.CLOUDAMQP_URL || "amqp://localhost";

class Service {
  constructor(options) {
    this.options = options || {};
  }

  create(data, params) {
    return new Promise((resolve, reject) => {
      const extension = mime.extension(data.mimeType);

      amqp.connect(AMQP_URL).then((conn) => {
        return conn.createChannel().then((ch) => {
          return new Promise((resolve) => {
            const corrId = uuid();

            function maybeAnswer(msg) {
              if (msg.properties.correlationId === corrId) {
                resolve(msg.content.toString());
              }
            }

            let ok = ch.assertQueue("", { exclusive: true })
              .then((qok) => { return qok.queue; });

            ok = ok.then((queue) => {
              return ch.consume(queue, maybeAnswer, { noAck: true })
                .then(function() { return queue; });
            });

            ok = ok.then((queue) => {
              const payload = Buffer.from(JSON.stringify({ audio: params.file.buffer, extension }))
              console.log(" [x] Requesting recognize");
              ch.sendToQueue("rpc_queue", payload, {
                correlationId: corrId, replyTo: queue
              });
            });
          });
        }).then((response) => {
          const hypothesis = JSON.parse(response).hypstr;
          console.log(` [.] Got '${hypothesis}'`);
          return resolve({
            data: {
              transcript: hypothesis,
              audioUrl: params.file.cloudStoragePublicUrl
            },
            _id: data.sentenceId
          });
        }).finally(() => { conn.close(); });
      }).catch(console.warn);
    });
  }
}

module.exports = function(options) {
  return new Service(options);
};

module.exports.Service = Service;
