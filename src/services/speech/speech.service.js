// Initializes the `speech` service on path `/speech`
const createService = require("./speech.class.js");
const hooks = require("./speech.hooks");
const filters = require("./speech.filters");

module.exports = function() {
  const app = this;
  const paginate = app.get("paginate");

  const options = {
    name: "speech",
    paginate
  };

  // Initialize our service with any options it requires
  app.use("/speech", createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service("speech");

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
