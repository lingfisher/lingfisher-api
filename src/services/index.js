const sentences = require("./sentences/sentences.service.js");
const speech = require("./speech/speech.service.js");
const users = require('./users/users.service.js');
module.exports = function() {
  const app = this; // eslint-disable-line no-unused-vars
  app.configure(sentences);
  app.configure(speech);
  app.configure(users);
};
